/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.net.InetAddress;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JEditorPane;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;

/**
 * Clase para enviar/recibir los mensajes y administrar las caracteristicas del cliente
 * @author juampa
 */
public class Cliente extends Conexion implements Runnable
{
    BigInteger pq, e; //Claves públicas recibidas del servidor
    RSA ras= new RSA(); //Clase con el metodo de desencriptado RSA para la clave a usar para Cifrado de Vigenere
    /**
     * Constructor del cliente
     * @param server la IP del servidor a conectarse
     * @param salida donde se mostrará la conversación
     * @param miNombre el nombre del usuario del cliente
     * @param cambios salida para mostrar la explicación del cifrado/descifrado
     * @throws IOException Captura de errores
     */
    public Cliente(InetAddress server, JTextPane salida, String miNombre, JTextPane cambios) throws IOException{
        super(server,salida, cambios);
        
        // Se recibe el nombre del servidor
        entrada= new DataInputStream(cs.getInputStream());
        String temp=entrada.readUTF();
        // Se verifica si se envió el nombre "Yo" desde el servidor, si no es así, se cambia el nombre de este para la conversación
        if(!temp.equals(this.miNombre))
            nombreContraparte=temp;
        // Se envia el nombre del cliente, al servidor
        if(miNombre.trim().length()!=0)
            this.miNombre=miNombre;
        this.salida=new DataOutputStream(cs.getOutputStream());
        this.salida.writeUTF(this.miNombre);
        //Recepcion de claves públicas del servidor
        e=BigInteger.valueOf(Long.parseLong(entrada.readUTF()));
        pq=BigInteger.valueOf(Long.parseLong(entrada.readUTF()));
        //Envio de claves publicas del cliente
        this.salida.writeUTF(ras.getE().toString());
        this.salida.writeUTF(ras.getPQ().toString());
        // Muestra las claves públicas recibidas del servidor
        JOptionPane.showMessageDialog(null, "pq= "+pq.toString()+"\ne= "+e.toString(),"Claves Recibidas",JOptionPane.INFORMATION_MESSAGE);
        //Recepcion de la clave de encriptación de Vegenere, cifrada con RAS
        CLAVECIF=entrada.readUTF();
        //Muestra la clave de Vigenere, cifrada con RSA, en consola
        System.out.println("Clave vigenere cifrada: "+ CLAVECIF);
        //Descrifrado de la clave para Vigenere, con RSA, y lo muestra en consola
        CLAVE=ras.descifrarV2(/*entrada.readUTF()*/CLAVECIF);
        System.out.println("Clave vigenere descifrada: "+CLAVE);
        cambios.setText(ras.getDesarrolloRSA());
    }
    /**
     * Se comprueba si hay una conexión activa
     * @return si esta conectado al servidor se manda "true"
     */
    public boolean isConectado(){
        return cs.isConnected();
    }
    /**
     * Enviar mensajes al servidor
     * @param msj el mensaje a enviar
     * @return el mensaje con el nombre de usuario que lo mando
     * @throws IOException Captura de errores
     */
    public String enviar(String msj) throws IOException{
        salida = new DataOutputStream(cs.getOutputStream());
        //Se encripta el mensaje original
        String encriptado=seguridad.Encriptar(msj.trim(), CLAVE);
        // Se envia el mensaje encriptado
        salida.writeUTF(encriptado);
        //Se agrega el mensaje a la conversación
        pane.setText(pane.getText()+miNombre+": "+msj+"\n");
        return miNombre+": "+msj;
    }
    /**
     * De desconecta del servidor, y se envía una señal de desconexión
     * @throws IOException Captura de errores
     */
    public void desconectar() throws IOException{
        //Envia la señal de desconexión
        salida.writeUTF(DESCONECTAR);
        //Se cierra la conexión
        cs.close();
    }
    /**
     * Metodo que se ejecuta en background para verificar si hay mensajes nuevos, y mostrarlos (se ejecuta en un hilo)
     */
    @Override
    public void run(){
        while(true){
            try{
                //Se lee el mensaje enviado del servidor
                String lectura=entrada.readUTF();
                //Se comprueba si no es la señal de desconexión0
                if(lectura.equals(DESCONECTAR)){
                   desconectar();
                   //Se envia un mensaje de desconexión del otro lado y se rompe el ciclo infinito de escucha
                   JOptionPane.showMessageDialog(null, "Desconectado");
                   break;
                }
                //Se desencripta el mensaje
                String desencriptado=seguridad.Desencriptar(lectura, CLAVE);
                //Se agrega el mensaje desencriptado a la conversación
                pane.setText(pane.getText()+nombreContraparte+": "+desencriptado+"\n");
                cambios.setText(seguridad.getProcedimiento_Des_Encriptado());
            }catch(IOException ex){
                Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public BigInteger getPq() {
        return pq;
    }

    public BigInteger getE() {
        return e;
    }
    public RSA getRSA(){
        return ras;
    }
    public String getClave(){
        return CLAVE;
    }
    public String getClaveCifrada(){
        return CLAVECIF;
    }
    public Cifrado_Simetrico_Vigenere getVigenere(){
        return seguridad;
    }
}
