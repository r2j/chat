package Clases;


import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import javax.swing.JTextPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Clase para manejar las conexiones, con las propiedades comunes cliente-servidor
 * @author juampa
 */
public class Conexion
{
    private final int PUERTO = 1234; //Puerto para la conexión
    protected final String DESCONECTAR=""; //Señal de desconexión a enviar
    protected String CLAVE="Ola k ace?", CLAVECIF="", mensaje; //Mensajes entrantes (recibidos)
    protected ServerSocket ss; //Socket del servidor
    protected Socket cs; //Socket del cliente
    protected DataOutputStream salida; //Flujo de datos de salida
    protected DataInputStream entrada; //Flujo de datos de entrada
    //protected JEditorPane pane; //Salida del chat
    protected JTextPane pane, cambios; //Salida para mostrar el proceso detalladamente
    protected Cifrado_Simetrico_Vigenere seguridad= new Cifrado_Simetrico_Vigenere(); //Crea un objeto para cifrar con el Cifrado de Vigenere
    protected String nombreContraparte="Otro Usuario", miNombre="Yo"; //Nombres por default de los usuarios en el chat
    /**
     * Inicializa la conexion para el servidor
     * @param salida salida de los mensajes
     * @param cambios salida para mostrar la explicación del cifrado/descifrado
     * @throws IOException Captura de errores
     */
    public Conexion(JTextPane salida, JTextPane cambios) throws IOException //Constructor
    {
            ss = new ServerSocket(PUERTO);//Se crea el socket para el servidor en puerto 1234
            cs = new Socket(); //Socket para el cliente
            this.pane=salida; //Se setea donde se van a mostrar los mensajes
            this.cambios=cambios;
    }
    /**
     * Inicializa la conexion para el cliente
     * @param IP La IP del servidor a conectarse
     * @param salida salida de los mensajes
     * @param cambios salida para mostrar la explicación del cifrado/descifrado
     * @throws IOException Captura de errores
     */
    public Conexion(InetAddress IP, JTextPane salida,JTextPane cambios) throws IOException{
        cs = new Socket(IP, PUERTO); //Socket para el cliente en localhost en puerto 1234
        this.pane=salida; //Se setea donde se van a mostrar los mensajes
        this.cambios=cambios;
    }
    
}
