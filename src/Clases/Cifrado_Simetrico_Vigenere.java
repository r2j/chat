/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 * Clase para hacer cifrado/descifrado de Vigenere
 * @author jonathanmiranda
 */
public class Cifrado_Simetrico_Vigenere {
    private int T_Mensaje_N_Cifrado, T_Mensaje_Cifrado;
    private char[] Char_Clave_Extendida, Char_Mensaje_N_Cifrado;
    private int[] Mensaje_Cifrado_Int;
    private String Procedimiento_Encriptado, Procedimiento_Des_Encriptado;

    public String getProcedimiento_Encriptado() {
        return Procedimiento_Encriptado;
    }

    public String getProcedimiento_Des_Encriptado() {
        return Procedimiento_Des_Encriptado;
    }

    
    
    
    /**
     * Conctructor por defecto de la clase
     */
    public Cifrado_Simetrico_Vigenere() {
        
    }
    
    /**
     * Metodo para encriptar, utiliza el codifo ascci como alfabeto, permitiendo el uso de 256 (del 0 al 255)
     * caracteres utilzando la funcion (M[i] + C[i])mod 255.
     * @param Mensaje mensaje original que sera encriptado
     * @param Clave Clave con la cual el mensaje sera encripatado
     * @return una cadena con el mensaje ya cifrado
     */
    public String Encriptar(String Mensaje, String Clave){
       String Encriptado="", mtemp="";
       Char_Mensaje_N_Cifrado=Mensaje.toCharArray();//convierte el mensaje a un arreglo characteres 
       T_Mensaje_N_Cifrado=Char_Mensaje_N_Cifrado.length;//obtiene el numero de characteres en el mensje
       
       Char_Clave_Extendida=Crear_Clave_Extendida(Clave, T_Mensaje_N_Cifrado);
       
      Procedimiento_Encriptado="<html>"+"<pre>"+"<font color=\"red\">"+"<strong>Encriptado     </strong>"+"<font color=\"black\">"+"<strong>Clave: </strong>\""+Clave+"\"<br>";
      
        for (int i = 0; i < T_Mensaje_N_Cifrado; i++) {//ciclo que recorre todo el mensaje 
            int caracter =(Char_Clave_Extendida[i]+Char_Mensaje_N_Cifrado[i])%256;
            Procedimiento_Encriptado+="<font color=\"black\">"+"<strong>Clave     Mensaje    Operacion                Cifrado</strong><br>";
            if ((int) Char_Clave_Extendida[i] <=99){

                Procedimiento_Encriptado+="<font color=\"blue\">"+ (char) Char_Clave_Extendida[i] +"<font color=\"black\">" +"0"+(int)Char_Clave_Extendida[i]+ ";     ";
            }else{
                Procedimiento_Encriptado+="<font color=\"blue\">"+ (char) Char_Clave_Extendida[i] +"<font color=\"black\">" +(int)Char_Clave_Extendida[i]+ ";     ";
            }
            if ((int)Char_Mensaje_N_Cifrado[i]<=99) {
                Procedimiento_Encriptado+="<font color=\"blue\">"+ (char) Char_Mensaje_N_Cifrado[i] + "<font color=\"black\">" +"0"+(int)Char_Mensaje_N_Cifrado[i]+ ";     ";
            }else{
                Procedimiento_Encriptado+=""+"<font color=\"blue\">"+ (char) Char_Mensaje_N_Cifrado[i] + "<font color=\"black\">" +(int)Char_Mensaje_N_Cifrado[i]+ ";     ";
            }
            if(caracter<=99){
                Procedimiento_Encriptado+="<font color=\"black\">"+"("+"<font color=\"blue\">"+(int)Char_Clave_Extendida[i]+"<font color=\"black\">"+" + " +"<font color=\"blue\">"+(int) Char_Mensaje_N_Cifrado[i]+"<font color=\"black\">"+") % 256 ="+"<font color=\"blue\">" +(int) caracter+";     ";
                 Encriptado += "0"+caracter;
                Procedimiento_Encriptado+="<html>"+""+"<font color=\"blue\">"+ (char) caracter + "<html>"+""+"<font color=\"black\">"+"0"+(int)caracter+"<html>"+""+"<br>";
            }else{
                Procedimiento_Encriptado+="<font color=\"black\">"+"("+"<font color=\"blue\">"+(int)Char_Clave_Extendida[i]+"<font color=\"black\">"+" + " +"<font color=\"blue\">"+(int) Char_Mensaje_N_Cifrado[i]+"<font color=\"black\">"+") % 256 ="+"<font color=\"blue\">" +(int) caracter+";     ";
                 Encriptado += caracter+"";
                Procedimiento_Encriptado+="<html>"+""+"<font color=\"blue\">"+ (char) caracter + "<html>"+""+"<font color=\"black\">"+(int)caracter+"<html>"+""+"<br>";
            }
            mtemp=mtemp+Char_Mensaje_N_Cifrado[i];
            Procedimiento_Encriptado+="<html>"+""+"<font color=\"gray\">"+"<strong>"+"Mensaje Desencriptado: </strong><font color=\"gray\">"+ mtemp +"<br>";
            Procedimiento_Encriptado+="<html>"+""+"<font color=\"gray\">"+"<strong>Mensaje Encriptado: </strong><font color=\"gray\">"+ Encriptado +"<br>";
        }

       return Encriptado;
    }
    
    /**
     * Metodo que se encarga de desencriptar el mensaje, para este metodo existen dos casos.
     *  1) que la resta del el caracter E[i] del mensaje encriptado y el caracter C[i] sea mayor o igual a 0
     *      -en este casa solo se hace la operacion (E[i]+C[i]) mod 255
     *  2)que la resta del el caracter E[i] del mensaje encriptado y el caracter C[i] sea menor a 
     *       -en este casa solo se hace la operacion (E[i]+C[i]+255) mod 255
     * @param Mensaje mensaje encriptado
     * @param Clave clave que se utilizo para encriptar el mensaje original
     * @return cadena de texto con el mensjae original
     */
    public String Desencriptar(String Mensaje, String Clave){
        String Desencriptado="", mtemp="", mtemp2="";
        T_Mensaje_Cifrado = Mensaje.length()/3;//obtiene el numero de characteres en el mensjen encriptado
        Mensaje_Cifrado_Int = toIntArray(Mensaje);//convierte el mensaje encriptado a un arreglo characteres
        Char_Mensaje_N_Cifrado = new char[T_Mensaje_Cifrado];
        
        Char_Clave_Extendida=Crear_Clave_Extendida(Clave, T_Mensaje_Cifrado);
        
         Procedimiento_Des_Encriptado="<html>"+"<pre>"+"<font color=\"red\">"+"<strong>Desencriptado     </strong>"+"<font color=\"black\">"+"<strong>Clave: </strong>\""+Clave+"\"<br>";
        int cont =0;
        for (int i = 0; i < T_Mensaje_Cifrado; i++) {//ciclo que recorre todo el mensaje
           
            if(Mensaje_Cifrado_Int[i]-Char_Clave_Extendida[cont]<0){//verifica que si la resta es menor a 0
                Procedimiento_Des_Encriptado+="<font color=\"black\">"+"<strong>Clave     Cifrado    Operacion                 Mensaje</strong><br>";
                Procedimiento_Des_Encriptado+="<font color=\"blue\">"+ (char) Char_Clave_Extendida[i] +"<font color=\"black\">" +(int)Char_Clave_Extendida[i]+ ";     ";
                Procedimiento_Des_Encriptado+="<font color=\"blue\">"+ (char) Mensaje_Cifrado_Int[i] +"<font color=\"black\">" +(int)Mensaje_Cifrado_Int[i]+ ";     ";
                char caracter =(char) ((Mensaje_Cifrado_Int[i]-Char_Clave_Extendida[i])% 256);//ya que la resta es menor a 0 se le suma 255
                Char_Mensaje_N_Cifrado[i]=caracter;
                Procedimiento_Des_Encriptado+="<font color=\"black\">"+"("+"<font color=\"blue\">"+(int)Char_Clave_Extendida[i]+"<font color=\"black\">"+" - " +"<font color=\"blue\">"+(int) Mensaje_Cifrado_Int[i]+"<font color=\"black\">"+") % 256 ="+"<font color=\"blue\">" +(int) caracter+";     ";
                Procedimiento_Des_Encriptado+="<font color=\"blue\">"+ (char) Char_Mensaje_N_Cifrado[i] +"<font color=\"black\">" +(int)Char_Mensaje_N_Cifrado[i]+ "<br>";
                mtemp+= Mensaje_Cifrado_Int[i];
                mtemp2+= caracter;
            }else{
                Procedimiento_Des_Encriptado+="<font color=\"black\">"+"<strong>Clave     Cifrado    Operacion                        Mensaje</strong><br>";
                Procedimiento_Des_Encriptado+="<font color=\"blue\">"+ (char) Char_Clave_Extendida[i] +"<font color=\"black\">" +(int)Char_Clave_Extendida[i]+ ";     ";
                Procedimiento_Des_Encriptado+="<font color=\"blue\">"+ (char) Mensaje_Cifrado_Int[i] +"<font color=\"black\">" +(int)Mensaje_Cifrado_Int[i]+ ";     ";
                char caracter = (char) ((Mensaje_Cifrado_Int[i]-Char_Clave_Extendida[i]+256)% 256);//ya que la resta es mayor o igula a 0 se procede a hacer la operacion modular
                 Char_Mensaje_N_Cifrado[i]=caracter;
                 Procedimiento_Des_Encriptado+="<font color=\"black\">"+"("+"<font color=\"blue\">"+(int)Char_Clave_Extendida[i]+"<font color=\"black\">"+" - " +"<font color=\"blue\">"+(int) Mensaje_Cifrado_Int[i]+"<font color=\"black\">"+" + 256) % 256 ="+"<font color=\"blue\">" +(int) caracter+";     ";
                 Procedimiento_Des_Encriptado+="<font color=\"blue\">"+ (char) Char_Mensaje_N_Cifrado[i] +"<font color=\"black\">" +(int)Char_Mensaje_N_Cifrado[i]+ "<br>";
                 mtemp+= Mensaje_Cifrado_Int[i];
                 mtemp2+= caracter;
            }
            cont++;
            Procedimiento_Des_Encriptado+="<html>"+""+"<font color=\"gray\">"+"<strong>Mensaje Encriptado: </strong><font color=\"gray\">"+ mtemp +"<br>";
            Procedimiento_Des_Encriptado+="<html>"+""+"<font color=\"gray\">"+"<strong>"+"Mensaje Desencriptado: </strong><font color=\"gray\">"+ mtemp2 +"<br>";
        }
        
        Desencriptado = Char_A_String(Char_Mensaje_N_Cifrado);
        return Desencriptado;
    }
   
   /**
    * Metodo que se encarga de generar un arreglo del tamaño del mensaje, y lo llena con la clave repetida mente
    * @param Clave clave qeu se utilizara para encriptar o desencriptar el mensaje
    * @param largo largo del mensaje que se encriptara o desencriptara 
    * @return Arreglo con la clave extendida
    */
   private char[] Crear_Clave_Extendida(String Clave, int largo){
       char[] Clave_Extendida = new char[largo], Clave_Original=Clave.toCharArray();
       
       int cont=0;
       for (int i = 0; i < largo; i++) {
            if (cont == Clave_Original.length){
               cont=0;
           }
           Clave_Extendida[i]=Clave_Original[cont];
           cont++;
       }
       
       
       return Clave_Extendida;
   }
   
   /**
    * Convierte un arreglo de caracteres en un Srting
    * @param Arreglo Arreglo que se convertira a String
    * @return El arreglo como un string
    */
   private String Char_A_String(char[] Arreglo){
       String fin="";
       
       for (int i = 0; i < Arreglo.length; i++) {
           fin += (char) Arreglo[i];
       }
       
       return fin;
             
   }
   
   private int[] toIntArray(String Mensaje){
       int[] CharArray = new int[Mensaje.length()/3];
       int cont=0;
       for (int i = 0; i < Mensaje.length(); i+=3) {
           CharArray[cont]= Integer.parseInt(Mensaje.substring(i, i+3));
           cont++;
       }
       
       return CharArray;
   }
}