    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Random;

/**
 * Clase para hacer cifrado/descifrado RSA
 * @author Roberto Arriola
 */
public final class RSA {;
    private BigInteger p,q,e,d;
    private BigInteger pq;
    private String desarrolloRSA="";

    public BigInteger getE() {
        return e;
    }

    public BigInteger getPQ() {
        return pq;
    }

    public String getDesarrolloRSA() {
        return desarrolloRSA;
    }

    
    public RSA() {
        p=generarPrimo(4);
        q=generarPrimo(4);
        generarE(4);
        generarD();
                
    }
    
    public String cifrar(String mensaje,BigInteger pq,BigInteger e)
    {
        
        String mensajeCifrado="";
        int noCaracter;
        BigInteger C;
        for(int i=0;i<mensaje.length();i++)
        {
            noCaracter=mensaje.charAt(i);
            C=new BigInteger(""+noCaracter);
            C=C.modPow(e,pq);
            mensajeCifrado+=C.toString();
            System.out.println("CHAR: "+mensaje.charAt(i)+"   O: "+(int)mensaje.charAt(i)
            +"    CHAR:"+mensajeCifrado.charAt(i)+"     C:"+C);
        }
        return mensajeCifrado;
    }
    /**
     * Método que cifra por bloques (dos caracteres-8 digitos(2B)) el mensaje
     * @param mensaje mensaje a cifrar 
     * @param pq primer numero de la clave publica del receptor
     * @param e segundo numero de la clave publica del receptor
     * @return mensaje cifrado en una String de numeros
     */
    public String cifrarV2(String mensaje,BigInteger pq,BigInteger e)
    {
        desarrolloRSA="<html><font color=\"purple\">- - - - - - - - - Cifrado - - - - - - - - - - - - -<br>";
        desarrolloRSA+="<strong><font color=\"black\">Claves:</strong><br>";
        desarrolloRSA+="<font color=\"blue\"><strong>Públicas: </strong><font color=\"black\"><strong>PQ: </strong>"+pq.toString()+", <strong>E= </strong>"+e.toString()+"<br>";
        desarrolloRSA+="<font color=\"blue\"><strong>Privadas: </strong><font color=\"black\"><strong>P: </strong>"+p.toString()+", <strong>Q= </strong>"+q.toString()+", <strong>d= </strong>"+d.toString()+"<br>";
        String mensajeCifrado="";/**
         * String que contendra el mensaje cifrado
         */
        int noCaracter, contador=0;/**
         * noCaracter entero que almacenara el valor numerico de los caracteres del mensaje
         * contador de los bloques
         */
        String bloque="";
        BigInteger C=null;/**
         * bloque - String que almacenara el bloque que sera convertido
         * C - bigInteger que servira para cifrar el bloque
         */
        for(int i=0;i<mensaje.length();i++)
        {
            if(contador<2)
            {
                noCaracter=mensaje.charAt(i);
                desarrolloRSA+="<font color=\"black\">Caracter  <font color=\"red\">"+(char) noCaracter;
                desarrolloRSA+="<font color=\"black\">    Numero  <font color=\"red\">"+noCaracter+"<br>";
                if(noCaracter<1000)
                    bloque+="0";
                if(noCaracter<100)
                    bloque+="0";
                if(noCaracter<10)
                    bloque+="0";
                /**El contador, lleva el conteo de los caracteres en el bloque (actualmente 2)
                 * Si el valor entero del caracter es menor a 1000 se le suma un cero al final, si es menor a 100 se añade a otro
                 * y si es menor a 10, otro. Para conservar la integridad del bloque (dos caracteres - 8 digitos)
                 */
                bloque+=(int) noCaracter;
                desarrolloRSA+="<font color=\"black\">BLOQUE ACTUAL: ";
                desarrolloRSA+="<font color=\"blue\">"+bloque+"<br>";
                desarrolloRSA+="<font color=\"black\">* * * * * * * * * * * * * * * * * * * * *<br>";
                contador++;
                /**se añade el valor int del caracter
                 * y se aumenta el contador 
                 */
            }
            if(contador==2 || i==(mensaje.length()-1))
            {
                while(bloque.length()<8)
                {
                    bloque+="0";
                }
                desarrolloRSA+="<font color=\"black\">BLOQUE:  <font color=\"blue\">"+bloque+"<br>";
                /**
                 *Si el bloque ya esta comleto (2 caracteres)
                 * añade ceros al inicio del bloque si la longitud es menor a 8 (medida preventiva
                 * para conservar la integridad del bloque)
                 */
                C=new BigInteger(bloque);
                C=C.modPow(e,pq);
                bloque=C.toString();/**
                 * Se le asigna al BigInteger C el valor del bloque, a este se le aplica la elevación a "e" de la clave publica
                 * y se obtiene el resiudo de pq de la clave publica
                 * El bloque recibe el nuevo valor de C
                 */
                while(bloque.length()<8)
                {
                    bloque="0"+bloque;
                } 
                desarrolloRSA+="<font color=\"green\">BLOQUE CIFRADO:  <font color=\"red\">"+bloque+"<br>";
                desarrolloRSA+="<font color=\"purple\">- - - - - - - FIN DE BLOQUE - - - - - <br>";
                mensajeCifrado+=bloque;
                bloque="";
                contador=0;
                /**
                 * Se añade el bloque al mensaje cifrado (el bloque es una String de numeris)
                 * se reinicia el bloque y el contador
                 */
            }
        }
        desarrolloRSA+="MENSAJE CIFRADO <font color=\"black\">"+mensajeCifrado+"<br>";
        return mensajeCifrado;/**
         * 
         * retorna el mesnaje cifrado (String de numeros)
         */
    }
    /**
     * Funcion que descifra un mensaje utilizando la clave privada 
     * @param mensaje String de numeros resultado de cifrar con la clave publica
     * @return 
     */
    public String descifrarV2(String mensaje)
    {
        String mensajeDescifrado="";
        desarrolloRSA="<html><font color=\"green\">- - - - - - - - Descifrado- - - - - - - <br>";
        desarrolloRSA+="<strong><font color=\"black\">Claves:</strong><br>";
        desarrolloRSA+="<font color=\"blue\"><strong>Públicas: </strong><font color=\"black\"><strong>PQ: </strong>"+pq.toString()+", <strong>E= </strong>"+e.toString()+"<br>";
        desarrolloRSA+="<font color=\"blue\"><strong>Privadas: </strong><font color=\"black\"><strong>P: </strong>"+p.toString()+", <strong>Q= </strong>"+q.toString()+", <strong>d= </strong>"+d.toString()+"<br>";
        int contador=0;
        String bloque="";
        BigInteger M=null;
        /**
         * mensajeDescifrado - String que almacenara el mensaje descifrado
         * contador - int que llevara el conteo de los caracteres (numeros) para obtener bloques completos
         * (actualmente los bloques son de 2 caracteres, 8 digitos)
         * bloque - String almacenara los bloques de caracteres
         */
        for(int i=0;i<mensaje.length();i++)
        {           
            if(contador<8)
            {
                bloque+=mensaje.charAt(i);
                contador++;
                /**
                 * mientras el contador sea menor a 8 se sumaran todos los caracteres al bloque
                 * y este aumenta en 1
                 */
            }
            if(contador==8 || i==(mensaje.length()-1))
            {   
                /**
                 * si el contador llega al tamaño de digitos de bloque (actualmente 8)
                 * o el  iterador de la longitud del mensaje a descifrar es igual a la longitud menos 1
                 * quiere decir que el bloque esta terminado (o el mensaje se ha terminado)
                 */
                desarrolloRSA+="<html><font color=\"black\">BLOQUE CIFRADO: <font color=\"blue\">"+bloque+"<br>";
                M=new BigInteger(bloque);
                M=M.modPow(d,pq);
                bloque=M.toString();
                desarrolloRSA+="<html><font color=\"black\">BLOQUE DESCIFRADO: <font color=\"blue\">"+bloque+"<br>";
                /**
                 * El BigInteger M guardare el valor del bloque (que es una String de caracteres)
                 * Utilizando la clave privada (pq,d) descifra cada bloque utilizando (M^d)mod(pq)
                 * bloque guarda el nuevo valor de M
                 */
                while(bloque.length()<8)
                {
                    bloque="0"+bloque;
                } 
                mensajeDescifrado+=(char)Integer.parseInt(bloque.substring(0,4))+""+(char)Integer.parseInt(bloque.substring(4,8));
                desarrolloRSA+="<font color=\"black\">Caracter 1    <font color =\"red\">"+(char)Integer.parseInt(bloque.substring(0,4))+"<br>";
                desarrolloRSA+="<font color=\"black\">Caracter 2    <font color =\"red\">"+(char)Integer.parseInt(bloque.substring(4,8))+"<br>";
                desarrolloRSA+="<font color=\"purple\">* * * * * * * * * * FIN DE BLOQUE * * * * * * * * * * * *<br>";
                bloque="";
                contador=0;
                /** Si en caso, el bloque tienen una logitud menor a 8, se añaden ceros al inicio por la integridad 
                 * 
                 */ 
            }
        }
        desarrolloRSA+="<font color=\"black\">BLOQUE DESFIFRADO: <font color=\"blue\">"+mensajeDescifrado+"<br>";
        desarrolloRSA+="<font color=\"green\">* * * * * * * * * * * * * * * * * * * * * * * * *<br>";
        return mensajeDescifrado.trim();
    }
    public String descifrar(String mensaje)
    {
        String mensajeDescifrado="";
        
        int noCaracter;
        BigInteger M;
        for(int i=0;i<mensaje.length();i++)
        {
            noCaracter=mensaje.charAt(i);
            M=new BigInteger(""+noCaracter);
            M=M.modPow(d,pq);
            mensajeDescifrado+=(char)(Integer.parseInt(M.toString()));
            System.out.println("CHAR: "+mensaje.charAt(i)+"    O: "+(int)mensaje.charAt(i)
            +"    M: "+M+"    CHAR:"+mensajeDescifrado.charAt(i));
        }
        return mensajeDescifrado;
    }
    public void generarE(int digitos)
    {
        e=generarPrimo(digitos);
        BigInteger P=new BigInteger(""+p);
        BigInteger Q=new BigInteger(""+q);
        pq=Q.multiply(P);
        while(!primosRelativos(e,pq))
        {
            e=generarPrimo(digitos);
        }
        
    }
    public void generarD()
    {
        d=e.modInverse(p.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE)));
        /*BigInteger mayor=p.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE)), menor=e;
        BigInteger y1=BigInteger.ONE, y2=BigInteger.ZERO,  y=BigInteger.ZERO;
        BigDecimal cociente, residuo;
        while(menor.compareTo(BigInteger.ZERO)>0)
        { 
            cociente = new BigDecimal(mayor.divide(menor));
            residuo = new BigDecimal(mayor.mod(menor));
            y = y2.subtract(cociente.multiply(new BigDecimal(y1)).toBigInteger());
            mayor = menor;
            menor = residuo.toBigInteger();
            y2 = y1;            
            y1 = y;
        }
        d=y2;
        if(d.compareTo(BigInteger.ZERO)<0)
            d=p.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE)).add(d);*/
    }
     /**
     * Método que genera un numero primo de 'n' digitos
     * Genera 'n' digitos al azar y comprueba si el numero formado es primo
     * si lo es retorna el numero, si no vuelve a generarlo
     * @param digitos numero 'n' de digitos
     * @return el numero primo generado
     */
    public BigInteger generarPrimo(int digitos)
    {
        Random md=new Random();//objeto de tipo tandom
        int digito;//variable en la que se almacena el digito generado
        BigInteger numero=null;//variables de conteo de cifras y numero generado, respectivamente
        
        for(int i=0;i<digitos;i++){//ciclo desde 0 hasta el numero de digitos-1
            if(i==0)//si el contador es para el primer digito
            {
                digito=(int)(md.nextDouble()*9+1);//se genera un digito desde 1 hasta 9 (evitar un 0)
                numero=new BigInteger(""+digito);
            }
            else 
            {
                digito=(int)(md.nextDouble()*10);//se genera un digito entre 0 y 9
            
                numero=new BigInteger(numero.toString()+digito);
            }
            if(i==digitos-1)
            {
                if(!comprobarPrimo(numero))//se comprueba si el numero es primo-si no lo es se reinician las variables
                {
                    i=-1;
                    numero=null;
                }
            }
        }
        return numero;
    }
    /**
     * Función boleeana que comprueba si un numero es primo o no
     * se suman la cantidad de divisores que tenga el numero
     * @param numero numero que se desea comprobar
     * @return true si el numero de divisores es menor a 2 - false si es mayor
     */
    public boolean comprobarPrimo(BigInteger numero)
    {
        BigInteger contador=BigInteger.ONE;
        int divisores=0;
        while(divisores<=2 && numero.compareTo(contador)>=0)
        {
            if(numero.mod(contador)==BigInteger.ZERO)
                divisores++;
            contador=contador.add(BigInteger.ONE);
        }
        if(divisores==2)
            return true;
        else
            return false;
    }
     private boolean primosRelativos(BigInteger numero1, BigInteger numero2){
        BigInteger contador=new BigInteger("2");
        boolean primos=true;
        while(contador.compareTo(numero1)<=0)
        {
            if(numero1.mod(contador)==BigInteger.ZERO&&numero2.mod(contador)==BigInteger.ZERO) {
                    primos=false; 
            }
            contador=contador.add(BigInteger.ONE);
        }
        return primos;
    }
}
    

