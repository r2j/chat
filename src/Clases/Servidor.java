/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;

/**
 * Clase para enviar/recibir los mensajes y administrar las caracteristicas del servidor
 * @author juampa
 */
public class Servidor extends Conexion implements Runnable
{
    RSA ras= new RSA(); //Clase de cifrado/descifrado RSA, para enviar la clave a usar con el cifrado Vigenere (el cual se usará para cifrar los mensajes)
    BigInteger pq, e; //Claves publicas
    /**
     * Constructor por defecto de la clase
     * @param salida se define en donde se mostraran los mensajes
     * @param miNombre nombre del usuario de la PC servidor
     * @param cambios salida para mostrar la explicación del cifrado/descifrado
     * @throws IOException 
     */
    public Servidor(JTextPane salida, String miNombre, JTextPane cambios) throws IOException{
        super(salida, cambios);
        //Se verifica si se envió un nombre del servidor
        if(miNombre.trim().length()!=0)
            this.miNombre=miNombre;
    }
    /**
     * Se inicia el servidor
     * @return si se ha conectado exitosamente al cliente
     */
    public boolean startServer()//Método para iniciar el servidor
    {
        boolean conectado=false;
        try
        {
            System.out.println("Esperando..."); //Esperando conexión
            cs = ss.accept(); //Accept comienza el socket y espera una conexión desde un cliente
            conectado=true;//Se setea que ya se conectó
            System.out.println("Cliente en línea"); //En consola se muestra que ya se ha conectado
            
            // Se envia el nombre del servidor
            salida=new DataOutputStream(cs.getOutputStream());
            salida.writeUTF(miNombre);
            // Se recibe el nombre del cliente
            entrada= new DataInputStream(cs.getInputStream());
            String temp=entrada.readUTF();
            if(!temp.equals(miNombre))
                nombreContraparte=temp;
            //Se envian las claves públicas del servidor
            salida.writeUTF(ras.getE().toString());
            salida.writeUTF(ras.getPQ().toString());
            //Se reciben las claves publicas del cliente
            e=BigInteger.valueOf(Long.parseLong(entrada.readUTF()));
            pq=BigInteger.valueOf(Long.parseLong(entrada.readUTF()));
            //Se muestran las claves públicas recibidas
            JOptionPane.showMessageDialog(null, "pq= "+pq.toString()+"\ne= "+e.toString(),"Claves Recibidas",JOptionPane.INFORMATION_MESSAGE);
            //Se pide la clave de cifrado para Vigenere, el cual es utilizado para cifrar los mensajes
            CLAVE=JOptionPane.showInputDialog("Ingrese la clave de cifrado:");
            //Muestra en consola la clave ingresada
            System.out.println("Clave cifrado Vigenere enviada (sin cifrar): "+ CLAVE);
            //Cifra la clave para Vigenere, con RSA
            CLAVECIF=ras.cifrarV2(CLAVE, pq, e);
            //Muestra la clave cifrada en consola y luego la envia
            System.out.println("Clave cifrado Vigenere enviada (cifrada): "+ CLAVECIF);
            salida.writeUTF(/*ras.cifrarV2(CLAVE, pq, e)*/CLAVECIF);
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
        return conectado;
    }
    /**
     * Metodo para enviar mensajes al cliente
     * @param msj Mensaje a enviar
     * @return Mensaje con el usuario que lo envió
     * @throws IOException Captura de errores
     */
    public String enviar(String msj) throws IOException{
        //mensajes+=msj;
        salida = new DataOutputStream(cs.getOutputStream());
        //Se encripta el mensaje original
        String encriptado=seguridad.Encriptar(msj.trim(), CLAVE);
        //Se envia el mensaje encriptado
        salida.writeUTF(encriptado);
        //Se muestra el nuevo mensaje en el pane
        pane.setText(pane.getText()+miNombre+": "+msj+"\n");
        return miNombre+": "+msj;
    }
    /**
     * Se desconecta del cliente y se envía una señal de desconexión
     * @throws IOException Captura de errores
     */
    public void desconectar() throws IOException{
        //Envio de la señal de desconexión
        salida.writeUTF(DESCONECTAR);
        ss.close();
    }
    /**
     * Metodo que se ejecuta en background para verificar si hay mensajes nuevos, y mostrarlos (se ejecuta en un hilo)
     */
    @Override
    public void run() {
        while(true){
            try{
                //Se lee lo que se envió del cliente
                String lectura=entrada.readUTF();
                //Se comprueba si es el mensaje de desconexión
                if(lectura.equals(DESCONECTAR)){
                    desconectar();
                    JOptionPane.showMessageDialog(null, "Desconectado");
                    break;
                }
                String desencriptado=seguridad.Desencriptar(lectura, CLAVE);
                pane.setText(pane.getText()+nombreContraparte+": "+desencriptado+"\n");
                cambios.setText(seguridad.getProcedimiento_Des_Encriptado());
            }catch(IOException ex){
                Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public BigInteger getPq() {
        return pq;
    }
    public BigInteger getE() {
        return e;
    }
    public RSA getRSA(){
        return ras;
    }
    public String getClave(){
        return CLAVE;
    }
    public String getClaveCifrada(){
        return CLAVECIF;
    }
    public Cifrado_Simetrico_Vigenere getVigenere(){
        return seguridad;
    }
}